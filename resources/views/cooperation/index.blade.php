@extends('layout')

@section('content')
<div class="col-md-3"></div>
<div class="col-md-6">
    @if (session('success'))
    <div class="alert alert-success" role="alert">
        Congrats you have registred successfully! we will contact you soon.
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            Featured
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('add_cooperation') }}">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Name and family</label>
                    <input type="name" class="form-control" id="name" name="name" />
                </div>
                <div class="mb-3">
                    <label for="mobile" class="form-label">Mobile</label>
                    <input type="name" class="form-control" id="mobile" name="mobile" placeholder="09123456789" />
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com">
                </div>
                <p style="margin-bottom: 0;">Gender</p>
                <div class="form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="genderM" value="male" checked>
                    <label class="form-check-label" for="genderM">
                        Male
                    </label>
                </div>
                <div class="form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="genderF" value="female">
                    <label class="form-check-label" for="genderF">
                        Female
                    </label>
                </div>
                <div class="mb-3" style="margin-top: 10px;">
                    <label for="description" class="form-label">Description</label>
                    <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Register</button>
            </form>
        </div>
        <div class="card-footer">
            <a href="/" class="btn btn-danger">Back to home</a>
        </div>
    </div>
</div>
<div class="col-md-3"></div>
@endsection