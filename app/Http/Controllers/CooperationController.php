<?php

namespace App\Http\Controllers;

use App\Models\Cooperation;
use Illuminate\Http\Request;

class CooperationController extends Controller
{
    public function index(){
        return view('cooperation.index');
    }

    public function add(Request $request, Cooperation $cooperation){
        // Validation
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:cooperation',
            'mobile' => 'required|digits:11|unique:cooperation',
            'gender' => 'required',
            'description' => 'required',
        ]);

        // Add to database
        $cooperation->name = $request->name;
        $cooperation->email = $request->email;
        $cooperation->mobile = $request->mobile;
        $cooperation->gender = $request->gender;
        $cooperation->description = $request->description; 
        $cooperation->save();
        
        // Return to view
        return redirect()->route('cooperation')->with('success',true);
    }
}
